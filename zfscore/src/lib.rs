//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

#![cfg_attr(feature = "pedantic", warn(clippy::pedantic))]
#![warn(clippy::use_self)]
#![warn(clippy::map_flatten)]
#![warn(clippy::map_unwrap_or)]
#![warn(deprecated_in_future)]
#![warn(future_incompatible)]
#![warn(noop_method_call)]
#![warn(unreachable_pub)]
#![warn(missing_debug_implementations)]
#![warn(rust_2018_compatibility)]
#![warn(rust_2021_compatibility)]
#![warn(rust_2018_idioms)]
#![warn(unused)]
#![deny(warnings)]

use razor_nvpair as nvpair;

use nvpair::NvListError;

pub use crate::lzc::zfs_prop_t;
pub use dataset::DatasetCollectorBuilder;
pub use dataset::ZfsDatasetHandle;
pub use razor_zfscore_sys::zfs_type_t;

pub mod error;
pub mod lzc;

mod dataset;
mod libzfs;

pub type Result<T, E = error::CoreError> = ::std::result::Result<T, E>;
