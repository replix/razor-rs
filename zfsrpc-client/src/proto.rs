//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

#![allow(unreachable_pub, clippy::use_self)]
tonic::include_proto!("zfsrpc");
