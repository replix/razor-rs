//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::io;

fn main() -> io::Result<()> {
    tonic_build::configure()
        .format(false)
        .compile(&["zpool.proto"], &["../proto"])?;

    Ok(())
}
