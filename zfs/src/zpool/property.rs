//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

pub use allocated::Allocated;

use super::InvalidProperty;

mod allocated;
mod altroot;
mod ashift;
mod bootfs;
mod cachefile;
mod expandsize;
mod failmode;
mod health;
mod onoff;
mod version;
mod yesno;
