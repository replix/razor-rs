//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::ffi;

use razor_nvpair as nvpair;
use razor_zfscore::lzc;

use lzc::zfs_prop_t::*;

use super::property;
use super::Result;
use super::ZfsDatasetHandle;

pub use bookmark::Bookmark;
pub use filesystem::Filesystem;
pub use filesystem::FilesystemBuilder;
pub use snapshot::Snapshot;
pub use snapshot::SnapshotBuilder;
pub use volume::Volume;
pub use volume::VolumeBuilder;

mod bookmark;
mod filesystem;
mod snapshot;
mod volume;
