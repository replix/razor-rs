//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::fmt;
use std::ops;
use std::str;

use chrono::{DateTime, TimeZone, Utc};
use serde::{Deserialize, Serialize};

use crate::error;

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct TimeStamp(DateTime<Utc>);

impl fmt::Display for TimeStamp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl str::FromStr for TimeStamp {
    type Err = error::InvalidProperty;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse::<i64>()
            .map(|seconds| Utc.timestamp(seconds, 0))
            .map(TimeStamp)
            .map_err(|_| error::InvalidProperty::invalid_value(s))
    }
}

impl ops::Deref for TimeStamp {
    type Target = DateTime<Utc>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
