//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

pub use error::InvalidProperty;
pub use property::CheckSum;
pub use property::Compression;
pub use property::OnOff;
pub use property::OnOffNoAuto;
pub use property::TimeStamp;
pub use property::Type;
pub use property::VolMode;
pub use property::YesNo;

mod error;
mod property;
